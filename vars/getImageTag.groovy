def call() {

    def commitHash = sh(script: 'git log -1 --pretty=format:"%h"', returnStdout: true).trim()
    def commitDate = sh(script: 'git log -1 --date=short --pretty=format:"%cd"', returnStdout: true).trim().replaceAll("-","")

    return "${commitDate}-${commitHash}"
}
